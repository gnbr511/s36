const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

//CREATE SINGLE TASK
router.post('/create', (request, response) => {
	TaskController.createTask(request.body).then((result) => {
		response.send(result)
	})
})

//GET ALL TASKS
router.get('/', (request, response) => {
	TaskController.getAllTasks().then((result) => {
		response.send(result)
	})
})

//UPDATING A TASK
router.patch('/:id/update', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((result) => {
		response.send(result)
	})
})

//DELETE TASK
router.delete('/:id/delete', (request, response) => {
	TaskController.deleteTask(request.params.id).then((result) => {
		response.send(result)
	})
})



//ACTIVITY S36 starts here ...

//FIND SPECIFIC TASK
router.get('/:id', (request, response) => {
	TaskController.searchTask(request.params.id).then((foundTask) =>{
		response.send(foundTask)
	})
})

//UPDATE TASK STATUS
router.put('/:id/complete', (request, response) => {
	TaskController.updateTaskStatus(request.params.id).then((updatedTask) => {
		response.send(updatedTask)
	})
})

module.exports = router