const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
	let newTask = new Task({
		name: data.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error){
			console.log(error)
			return error
		}
		return savedTask
	})
}

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result
	})
}

module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}
		result.name = new_data.name

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}
			return updatedTask
		})
	})
}

module.exports.deleteTask = (task_id) => {	
	//NOTE: should use findByIdAndDelete to put a condition for catching error
	return Task.deleteOne({_id: task_id}).then((result, error) => {
		if(error){
			console.log(error)
			return error
		}
		return result
	})
}


//ACTIVITY S36 Modules and Parameterized Routes

//find specific task
module.exports.searchTask = (task_id) => {
	return Task.findById(task_id).then((foundTask) => {
		if(!foundTask){
			return 'No such task found!'
		}
		return foundTask
	})
}

//update task status
module.exports.updateTaskStatus = (task_id) => {
	return Task.findById(task_id).then((foundTask) => {
		if(!foundTask){
			return "No such task found!"
		}
		foundTask.status = "complete"
		return foundTask.save().then((updatedTask) => {
			if(!updatedTask){
				return 'No such task found!'
			}
			return updatedTask
		})
	})
}

//ACTIVITY S36 Modules and Parameterized Routes END