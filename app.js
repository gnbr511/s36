//IMPORTING THE DEPENDENCIES OR MODULES
const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

const taskRoute = require('./routes/taskRoutes')

//INITIALIZE DOT ENV
dotenv.config()

//SERVER SET UP
const app = express()
const port = 3003
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//MONGODB CONNECTION
mongoose.connect(`mongodb+srv://gnbr0511:${process.env.MONGODB_PASSWORD}@cluster0.h8bn8fo.mongodb.net/s36-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.error('Connection error.'))
db.on('open', () => console.log('We are connected to MongoDB!'))

//ROUTES
app.use('/tasks', taskRoute)

//SERVER LISTENING
app.listen(port, () => console.log(`Server running on localhost:${port}`))